# usr-local-bin

## Setup

Usually, `/usr/local/bin` is left empty by the system for admin/users to use it. If you do not use it for other purposes, you can setup this repo like this:

``` sh
sudo chown ":$(id -g)" /usr/local/bin; sudo chmod g+rwx /usr/local/bin
git clone https://gitlab.com/replicajune/usr-local-bin.git /usr/local/bin
```

## Usage

Check the description in each script to see what they do.

> Please be careful before using these scripts, some may use dependencies you don't have on your system. Among others, you shoud have : git, docker (or podman), parallel.. and maybe others if this tiny list is not up to date.
